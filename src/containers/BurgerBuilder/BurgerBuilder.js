import React, {Component} from 'react';
import Wrapper from "../../hoc/Wrapper";

class BurgerBuilder extends Component {
  render() {
    return (
      <Wrapper>
        <div>Burger will be here</div>
        <div>Build controls will be here</div>
      </Wrapper>
    )
  }
}

export default BurgerBuilder;